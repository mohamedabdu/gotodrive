//
//	TripResult.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class TripResult : Decodable{
    
	var category_id : Int?
	var category_name : String?
    var category_icon: String?
    var category_calculating_pricing: String?
	var client_comment : String?
	var client_rate : Int?
    var driver_comment : String?
    var driver_rate : Int?
	var commission : Double?
	var created_at : String?
    var country_tax:Double?
    var wallet:Double?
	var discount : Int?
	var distance : Double?
	var driver : TripDriver?
	var end_date : String?
	var fare_estimation : String?
	var from_lat : Double?
	var from_lng : Double?
	var from_location : String?
	var id : Int?
	var payment_method : Int?
    var payment_paid:Bool?
	var payment_method_text : String?
	var statistics : TripStatistic?
	var status : Int?
	var status_text : String?
	var tax : Double?
	var time_estimation : String?
	var to_lat : Double?
	var to_lng : Double?
	var to_location : String?
	var total_price : Double?
	var trip_price : Double?
    // var type : String?
	var trip_delivery_note : String?
    var requests: [TripRequest]?

}


class TripRequest:Decodable{
    var id:Int?
    var price:Int?
    var accept:Int?
    var type:String?
    var type_text:String?
    var driver:TripDriver?
}

class TripResultNotification : Decodable {
    
    var result: TripResult?
    public static func convertToModel(response: Data?) -> TripResultNotification {
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return TripResultNotification()
        }
    }
}
