//
//	SubCategoryResult.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 

class CompaniesModel : Decodable{
    
    var result:[Driver]?
    public static func convertToModel(response: Data?) -> CompaniesModel{
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return CompaniesModel()
        }
    }
}

class Driver : Decodable{
    
    var id:Int?
    var company_name:String?
    var company_description:String?
    var company_img:String?
    var company_contact:String?
    var admin_credit : Int?
    var subCategories:[SubCategoryChild]?
    var car : DriverCar?
    var car_license_img : String?
    var category_id : Int?
    var category_name : String?
    var driving_license_img : String?
    var his_credit : Int?
    var lat : String?
    var lng : String?
    var online : Bool?
    var statistics : DriverStatistic?
    var total_credit : Int?
    
}
class DriverStatistic : Decodable{
    var online_hours : Int?
    var today_earned : Int?
    var today_trips : Int?
}

