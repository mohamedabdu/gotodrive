//
//	ConfigCategory.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class ConfigCategory : Decodable{

	var description : String?
    var has_sub : Bool?
	var auto_accept : Bool?
    var has_level2:Bool?
	var id : Int?
	var img : String?
    var name : String?
	var calculating_pricing : String?
    


	public static func convertToModel(response: Data?) -> ConfigCategory{
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ConfigCategory() 
		}
 	}


}
