//
//	ChatModel.swift
//
//	Create by mohamed abdo on 11/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class ChatModel : Decodable {

	var result: [ChatResult]?
    var statusCode: Int?
    var statusText: String?

	public static func convertToModel(response: Data?) -> ChatModel {
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ChatModel() 
		}
 	}


}
