//
//	ChatResponse.swift
//
//	Create by mohamed abdo on 11/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class ChatResult: Decodable {
    var id: Int?
    var room_id : Int?
    var from_you : Bool?
    var message : String?
    var user : ChatUser?
    var created_at: String?
    var result: ChatResult?
    var trip_id: String?
    var sender_name: String?
    var sender_image: String?
    var type: Int?
    var from: Int?
    
	public static func convertToModel(response: Data?) -> ChatResult {
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ChatResult() 
		}
 	}


}
