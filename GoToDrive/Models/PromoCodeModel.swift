//
//	TripCurrentTripModel.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class PromoCodeModel : Decodable {

	var result : PromoCodeResult?
	var statusCode : Int?
	var statusText : String?


	public static func convertToModel(response: Data?) -> PromoCodeModel {
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return PromoCodeModel()
		}
 	}


}
class PromoCodeResult : Decodable {
    var id: Int?
    var code: String?
    var discount: Double?
}
