
import Foundation

class CompaniesViewModel:ViewModelCore {
    
    var companies:DynamicType = DynamicType<[Driver]>()

    func fetchData(category:Int) {
    
        delegate?.startLoading()

        let method = api(.companies,[category])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CompaniesModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                self.companies.value = data.result!
            }
        }
    }
   
}


