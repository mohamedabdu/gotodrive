
import Foundation

class TripViewModel:ViewModelCore {
    enum TripStatus:Int {
        case opened = 0
        case accepted = 1
        case canceled = 2
        case rejected = 3
        case arrived = 4
        case started = 5
        case completed = 6
        case collected = 7
    }
    
    var currentTrip:DynamicType = DynamicType<TripResult>()
    var drivers:DynamicType = DynamicType<[TripDriver]>()
    var lastTrip:DynamicType = DynamicType<TripResult>()
    var estimateTrip:DynamicType = DynamicType<TripResult>()
    var created:DynamicType = DynamicType<TripResult>()
    var cancelMessage:DynamicType = DynamicType<String>()
    var acceptPrice:DynamicType = DynamicType<String>()
    var message:DynamicType = DynamicType<String>()
    var promoCodeHandler:DynamicType = DynamicType<PromoCodeResult>()
    var chatHandler:DynamicType = DynamicType<[ChatResult]>()

    func current(_ lat:Double ,_ lng:Double) {
    
        delegate?.startLoading()
        ApiManager.instance.paramaters["lat"] = lat
        ApiManager.instance.paramaters["lng"] = lng
        ApiManager.instance.connection(.current_trip, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
            else if(data.result == nil){
                self.currentTrip.value = TripResult()
                
            }
            if data.drivers != nil {
                self.drivers.value = data.drivers!
            }
            if data.rate_last_trip != nil {
                self.lastTrip.value = data.rate_last_trip!
            }
        }
    }
    func estimate(from_lat:Double , from_lng:Double , to_lat:Double , to_lng:Double ) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["from_lat"] = from_lat
        ApiManager.instance.paramaters["from_lng"] = from_lng
        ApiManager.instance.paramaters["to_lat"] = to_lat
        ApiManager.instance.paramaters["to_lng"] = to_lng
        ApiManager.instance.connection(.estimate_trip, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.estimateTrip.value = data.result!
            }
        }
    }
    func create(paramters:[String:Any]) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(.make_request, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.created.value = data.result!
            }
        }
    }
    func createRequestCompany(paramters:[String:Any]) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(.make_request_company, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.created.value = data.result!
            }
        }
    }
    func changeDestination(lat:Double,lng:Double) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["to_lat"] = lat
        ApiManager.instance.paramaters["to_lng"] = lng
        ApiManager.instance.connection(.change_destination, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func cancel(reason:Int) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["cancel_reason_id"] = reason
        ApiManager.instance.connection(.cancel_request, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if(data.message != nil){
                self.cancelMessage.value = data.message!
            }
        }
    }
    func rate(trip:Int,rate:Int,comment:String?) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["rate"] = rate
        if comment != nil {
            ApiManager.instance.paramaters["comment"] = comment!
        }
        let method = api(.rate,[trip])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.message.value = translate("success")
        }
    }
    func acceptPrice(request:Int) {
        
        delegate?.startLoading()
        let method = api(.accept_price,[request])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.acceptPrice.value = data.message
        }
    }
    func rejectPrice(request:Int) {
        delegate?.startLoading()
        let method = api(.reject_price,[request])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.cancelMessage.value = data.message
        }
    }
    func finishRequestCompany() {
        delegate?.startLoading()
        let method = api(.finish_request)
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.message.value = translate("success")
        }
    }
    func changePaymentMethod(paymentMethod:String) {
        delegate?.startLoading()
        let method = api(.change_payment)
        ApiManager.instance.paramaters["payment_method"] = paymentMethod
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.message.value = translate("success")
        }
    }
    func promoCode(code: String) {
        delegate?.startLoading()
        ApiManager.instance.paramaters["promo_code"] = code
        ApiManager.instance.connection(.promoCode, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = PromoCodeModel.convertToModel(response: response)
            self.promoCodeHandler.value = data.result
        }
    }
    func chatRoom(userID: Int?, trip: Int?) {
        guard let userID = userID, let trip = trip else { return }
        delegate?.startLoading()
        ApiManager.instance.paramaters["trip_id"] = trip
        ApiManager.instance.paramaters["user_id"] = userID
        ApiManager.instance.connection(.chat, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ChatModel.convertToModel(response: response)
            self.chatHandler.value = data.result
        }
    }
    func sendMessage(userID: Int?, trip: Int?, message: String?) {
        guard let userID = userID, let trip = trip, let message = message else { return }
        delegate?.startLoading()
        ApiManager.instance.paramaters["trip_id"] = trip
        ApiManager.instance.paramaters["user_id"] = userID
        ApiManager.instance.paramaters["message"] = message
        ApiManager.instance.connection(.send_message, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = ChatModel.convertToModel(response: response)
            //            self.chatHandler.value = data.result
        }
    }
}


