
import Foundation

class UserViewModel:ViewModelCore {
    
    
    var model:DynamicType = DynamicType<UserRoot>()
    var message:DynamicType = DynamicType<String>()
    var history:DynamicType = DynamicType<[TripResult]>()
    var notifications:DynamicType = DynamicType<[NotificationsResult]>()
    
    func fetchData() {
        delegate?.startLoading()
        ApiManager.instance.connection(.configs, type: .get) { (response) in
            self.delegate?.stopLoading()
            
        }
    }
    
    
    func login(username:String , password:String) {
        ApiManager.instance.paramaters["username"] = username
        ApiManager.instance.paramaters["password"] = password
        
        delegate?.startLoading()
        ApiManager.instance.connection(.login, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    
    func register(paramters:[String:String]) {
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.register, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func social(paramters:[String:String]) {
        ApiManager.instance.paramaters = paramters
    
        delegate?.startLoading()
        ApiManager.instance.connection(.social, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func activate(code:Int) {
        
        let method = api(.activate,[code])
        delegate?.startLoading()
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ActivateModel.convertToModel(response: response)
            if data.message != nil {
              self.message.value = data.message
            }
        }
    }
    func update(paramters:[String:String]) {
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.update, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.result != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func forget(email:String) {
        ApiManager.instance.paramaters["email"] = email
        
        delegate?.startLoading()
        ApiManager.instance.connection(.forget_password, type: .post) { (response) in
            self.delegate?.stopLoading()
            self.message.value = translate("password_reset_success")
        }
    }
    func reset(paramaters:[String:Any]) {
        ApiManager.instance.paramaters = paramaters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.reset_password, type: .post) { (response) in
            self.delegate?.stopLoading()
            self.message.value = translate("password_reset_success")
        }
    }
    func historyTrips() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.history, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = HistoryModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                self.history.value = data.result
            }
        }
    }
    func fetchNotifications() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.notifications, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = NotificationsModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                self.notifications.value = data.result
            }
        }
    }
}


