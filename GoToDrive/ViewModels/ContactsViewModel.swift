
import Foundation

class ContactsViewModel:ViewModelCore {
    
    var contacts:DynamicType = DynamicType<[ContactsResult]>()
    var user:DynamicType = DynamicType<UserRoot>()
    var message:DynamicType = DynamicType<String>()

    func fetchData(getDefaultContact:Bool = false) {
    
        delegate?.startLoading()
        if(getDefaultContact){
            ApiManager.instance.paramaters["default_contact"] = 1
        }
        ApiManager.instance.connection(.client_contacts, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ContactsModel.convertToModel(response: response)
            if data.result != nil {
                self.contacts.value = data.result!
            }
        }
    }
    
    func create(json:String) {
        delegate?.startLoading()
        ApiManager.instance.paramaters["contacts"] = json
        ApiManager.instance.connection(.store_contacts, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = ContactsModel.convertToModel(response: response)
            if data.result != nil {
                self.contacts.value = data.result!
            }
        }
    }
    
    func defaultContact(contact:Int) {
        
        delegate?.startLoading()
        let method = api(.default_contact , [contact])
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if data.result != nil {
                self.message.value = data.result!
            }
        }
    }
    
    func delete(contact:Int) {
        
        delegate?.startLoading()
        let method = api(.client_contacts , [contact])
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if data.result != nil {
                self.message.value = data.result!
            }
        }
    }
    

}


