//
//  HistoryCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

protocol HistoryCompanyCellDelegate :class {
    func finishTrip(path:Int)
}
class HistoryCompanyCell: UITableViewCell , CellProtocol {

    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var finishBtn: UIButton!
    
    weak var delegate:HistoryCompanyCellDelegate?
    
    func setup() {
        guard let data = model as? TripResult else { return }
        userImage.setImage(url: data.driver?.driver?.company_img)
        createdDate.text = data.created_at
        totalPrice.text = translate(data.fare_estimation, "SAR", true)
        paymentMethod.text = data.payment_method_text
        status.text = data.status_text
        if data.status != nil && (data.status == TripViewModel.TripStatus.canceled.rawValue || data.status == TripViewModel.TripStatus.rejected.rawValue) {
            status.textColor = UIColor.colorRGB(red: 223, green: 71, blue: 83)
        }else{
            status.textColor = UIColor.colorRGB(red: 112, green: 178, blue: 102)
        }
        
        if data.status != nil && data.status! != 1 {
            finishBtn.isHidden = true
        }
        if data.status != nil && data.status! == 1{
            finishBtn.isHidden = false
        }
    }
    
    @IBAction func finish(_ sender: Any) {
        self.delegate?.finishTrip(path: self.path!)
    }
}
