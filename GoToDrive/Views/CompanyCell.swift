//
//  CompanyCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 9/1/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class CompanyCell: UICollectionViewCell , CellProtocol {

    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    
    func setup() {
        guard let data = model as? Driver else { return }
        companyName.text = data.company_name
        companyImage.setImage(url: data.company_img)
    }
}
