//
//  ContactCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

protocol ContactCellDelegate:class {
    func select(path:Int)
    func disSelect(path:Int)
}
class ContactCell: UITableViewCell , CellProtocol{

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactMobile: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    var fromEmergencyContactList:Bool = false
    weak var delegate:ContactCellDelegate?
    var checked:Bool = false
    func setup() {
        if checked{
            checkBtn.setImage(#imageLiteral(resourceName: "checkBox"), for: .normal)
            checkBtn.borderWidth = 0
        }else{
            checkBtn.setImage(nil, for: .normal)
            checkBtn.borderWidth = 1.5
        }
        guard let data = model as? ContactsResult else { return }
        contactName.text = data.name
        contactMobile.text = data.mobile
        
        
        if data.image != nil {
            contactImage.setImage(url: data.image)
        }else if data.imageData != nil {
            guard let imageData = data.imageData else { return }
            contactImage.image = UIImage(data: imageData)
        }
        
        
    }
    
    @IBAction func check(_ sender: Any) {
        if checked{
            if !fromEmergencyContactList {
                checked = false
                checkBtn.setImage(nil, for: .normal)
                checkBtn.borderWidth = 1.5
            }
            self.delegate?.disSelect(path: self.path!)
        }else{
            if !fromEmergencyContactList {
                checked = true
                checkBtn.setImage(#imageLiteral(resourceName: "checkBox"), for: .normal)
                checkBtn.borderWidth = 0
            }
            self.delegate?.select(path: self.path!)
        }
    }
}
