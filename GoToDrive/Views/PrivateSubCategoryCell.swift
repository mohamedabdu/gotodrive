//
//  PrivateSubCategoryCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 12/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class PrivateSubCategoryCell: UICollectionViewCell , CellProtocol {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var selectedBtn: RadioButton!
    
    var categorySelected:Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup() {
        guard let data = model as? SubCategoryResult else { return }
        categoryImage.setImage(url: data.image)
        categoryName.text = data.name
      
        if categorySelected {
            selectedBtn.isHidden = false
            selectedBtn.select()
        }else{
            selectedBtn.isHidden = true
            selectedBtn.deselect()
        }
    }

}
