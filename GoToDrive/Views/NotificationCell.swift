//
//  NotificationCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell , CellProtocol {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    
    func setup() {
        guard let data = model as? NotificationsResult else { return }
        title.text = data.title
        body.text = data.body
    }
}
