//
//  HistoryCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell , CellProtocol {

    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var destinationLocation: UILabel!
    @IBOutlet weak var carInfo: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var note: UILabel!
    
    
    func setup() {
        guard let data = model as? TripResult else { return }
        userImage.setImage(url: data.driver?.image)
        userName.text = "\(data.driver?.first_name ?? "") \(data.driver?.last_name ?? "")"
        carInfo.text = "\(data.driver?.driver?.car?.brand_name ?? "") \(data.driver?.driver?.car?.model_name ?? "")"
        destinationLocation.text = data.to_location
        createdDate.text = data.created_at
        totalPrice.text = "\(data.fare_estimation ?? "") \(translate("SAR"))"
        paymentMethod.text = data.payment_method_text
        status.text = data.status_text
        note.text = "\(translate("note_:")) \(data.trip_delivery_note ?? "")"
        if data.status != nil && (data.status == TripViewModel.TripStatus.canceled.rawValue || data.status == TripViewModel.TripStatus.rejected.rawValue) {
            status.textColor = UIColor.colorRGB(red: 223, green: 71, blue: 83)
        }else{
            status.textColor = UIColor.colorRGB(red: 112, green: 178, blue: 102)
        }
    }
    
}
