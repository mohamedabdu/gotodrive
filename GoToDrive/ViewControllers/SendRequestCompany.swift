//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class SendRequestCompany:BaseController {
    enum SelectionLocation {
        case origin
        case destination
    }
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var requestNote: UITextView!
    @IBOutlet weak var typeGood: UITextField!
    @IBOutlet weak var to: UITextField!
    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var number_of_ressamble: UITextField!
    
    var company:Driver!
    var category:Int!
    var subCategory:Int!
    var fromLat:Double?
    var fromLng:Double?
    var toLat:Double?
    var toLng:Double?
    
    var currentSelectionLocation:SelectionLocation = .origin
    var viewModel:TripViewModel?
    var mapHelper:GoogleMapHelper?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = TripViewModel()
        viewModel?.delegate = self
        mapHelper = GoogleMapHelper()
        mapHelper?.delegate = self
        
        from.addPaddingLeft(5)
        to.addPaddingLeft(5)
        typeGood.addPaddingLeft(5)
        if category == 4 {
            number_of_ressamble.addPaddingLeft(5)
            number_of_ressamble.isHidden = false
        }else{
            number_of_ressamble.isHidden = true
        }
        companyName.text = company.company_name
        companyImage.setImage(url: company.company_img)
    }
    override func notifySetting() {
    }
    override func bind() {
        viewModel?.created.bind({ (data) in
            if data.id != nil {
                let vc = self.pushViewController(BookingCompanySuccessPOP.self)
                vc.delegate = self
                vc.word = translate("the company will respond to your request for the price and is required by you to implement")
                self.pushPop(vc: vc)
            }
        })
    }
    override func validation() -> Bool {
        if fromLat == nil || fromLng == nil {
            SnackBar(message: translate("please_select_from_location"), duration: .short)
            return false
        }else if toLat == nil || toLng == nil {
            SnackBar(message: translate("please_select_to_location"), duration: .short)
            return false
        }else {
            let validate = Validation(textFields: [from , to , typeGood])
            return validate.success
        }
    }
    @IBAction func toLocation(_ sender: Any) {
        currentSelectionLocation = .destination
        self.mapHelper?.search()
    }
    @IBAction func fromLocation(_ sender: Any) {
        currentSelectionLocation = .origin
        self.mapHelper?.search()
        
    }
    @IBAction func sendRequest(_ sender: Any) {
        if self.validation() {
            var paramters:[String:Any] = [:]
            paramters["from_lat"] = fromLat
            paramters["from_lng"] = fromLng
            paramters["to_lat"] = toLat
            paramters["to_lng"] = toLng
            paramters["company_id"] = company.id!
            paramters["category_id"] = category
            paramters["sub_category_id"] = subCategory
            paramters["trip_special_description"] = typeGood.text
            paramters["trip_special_note"] = requestNote.text
            if category == 4 {
                paramters["trip_special_reassemble"] = number_of_ressamble.text
            }
            viewModel?.createRequestCompany(paramters: paramters)
        }
    }
}

extension SendRequestCompany:GoogleMapHelperDelegate {
    func locationCallback(lat: Double, lng: Double) {
        if currentSelectionLocation == .origin {
            fromLat = lat
            fromLng = lng
        }else{
            toLat = lat
            toLng = lng
        }
    }
    func locationCallback(address: String?) {
        if currentSelectionLocation == .origin {
            from.text = address
        }else{
            to.text = address
        }
    }
}

extension SendRequestCompany:BookingCompanySuccessDelegate {
    func successApply() {
        let vc = pushViewController(Home.self)
        push(vc)
    }
    func successCancel() {
        let vc = pushViewController(Home.self)
        push(vc)
    }
}
