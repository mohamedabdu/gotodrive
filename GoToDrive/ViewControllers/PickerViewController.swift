//
//  IntroController.swift
//  Fennex
//
//  Created by mohamed abdo on 3/18/19.
//  Copyright © 2019 Onnety. All rights reserved.
//

import Foundation

class PickerViewController: BaseController {
    enum PickerType {
        case items
        case date
        case time
    }
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var pickerView: UIPickerView! {
        didSet {
            pickerView.delegate = self
            pickerView.dataSource = self
        }
    }
    
    var source: [Any] = []
    var titleClosure: ((Int) -> String?)?
    var didSelectPath: ((Int) -> ())?
    var didSelectItem: ((Any) -> ())?
    var didSelectDate: ((String?) -> ())?
    var type: PickerType = .items
    private var date: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func bind() {
        
    }
    func setup() {
        if type == .items {
            datePicker.isHidden = true
        } else {
            datePicker.isHidden = false
            pickerView.isHidden = true
        }
        if type == .date {
            datePicker.datePickerMode = .date
        } else if type == .time {
            datePicker.datePickerMode = .time
        }
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func agree(_ sender: Any) {
        if type == .date {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            date = formatter.string(from: datePicker.date)
            self.dismiss(animated: true) { [weak self] in
                self?.didSelectDate?(self?.date)
            }
            
        } else if type == .time {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            date = formatter.string(from: datePicker.date)
            self.dismiss(animated: true) { [weak self] in
                self?.didSelectDate?(self?.date)
            }
        } else {
            let selectedRow = pickerView.selectedRow(inComponent: 0)
            self.dismiss(animated: true) { [weak self] in
                if let path = self?.source[safe: selectedRow] {
                    self?.didSelectPath?(selectedRow)
                    self?.didSelectItem?(path)
                }
            }
        }
        
    }
    
}

extension PickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return source.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titleClosure?(row)
    }
}
