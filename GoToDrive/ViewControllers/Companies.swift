//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Companies:BaseController {
    @IBOutlet weak var companiesCollection: UICollectionView!
    
    var viewModel:CompaniesViewModel?
    var category:Int!
    var companies:[Driver] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        companies.removeAll()
    }
    
    func setup() {
        companiesCollection.delegate = self
        companiesCollection.dataSource = self
        viewModel = CompaniesViewModel()
        viewModel?.delegate = self
        viewModel?.fetchData(category: category)
    }

    override func bind() {
        viewModel?.companies.bind({ (data) in
            self.companies.append(contentsOf: data)
            self.companiesCollection.reloadData{
                self.companiesCollection.animateZoom(0.10)
            }
           
        })
    }
}


extension Companies:UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.didEndDragging() && viewModel!.runPaginator() {
            self.viewModel?.fetchData(category: category)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width/2-10, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.companies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard var cell = collectionView.cell(type: CompanyCell.self, indexPath) else { return UICollectionViewCell() }
        cell.model = self.companies[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = pushViewController(CompanyCategories.self)
        vc.company = companies[indexPath.item]
        if let categories = companies[indexPath.item].subCategories {
            vc.categories = categories
        }
        vc.categoryParentID = category
        push(vc)
    }
    
}
