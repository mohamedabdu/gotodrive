//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import WebKit
import UIKit


protocol PaymentRefreshDelegate:class {
    func faildPayment()
    func successPayment()
    func closePayment()
    func checkPaymentOnline()

}

extension PaymentRefreshDelegate {
    func closePayment(){
        
    }
    func faildPayment(){
        
    }
    func successPayment(){
        
    }
    func checkPaymentOnline(){
        
    }
}
class PaymentOnline:BaseController, WKUIDelegate , PaymentRefreshDelegate {
    
    static var tripURL = "http://nashmit.com/payment/transaction/process?trip_id="
    static var requestURL = "http://nashmit.com/payment/transaction/requestAdmin/process?request_id="
    
    static var paymentSuccess:Bool = false
    var requestID:String?
    weak var delegate:PaymentRefreshDelegate?
    
    var viewModel:TripViewModel?
    var trip:TripResult?
    var timer:TimeHelper?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        // let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.paymentDelegate = nil
        viewModel = nil
        timer?.stopTimer()
        timer = nil
        
    }
    
    
    func setup() {
        self.timer = TimeHelper(seconds: 8, closure: updateMap)
        viewModel = TripViewModel()

        // let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.paymentDelegate = self
        let webView = WKWebView(frame: CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height))
        let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        backBtn.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
        backBtn.addTarget(self, action:#selector(cancel), for: .touchUpInside)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.30)
        self.view.addSubview(backBtn)
        self.view.addSubview(webView)
        let paymentURL = "\(PaymentOnline.tripURL)\(requestID ?? "")"
        let url = URL(string: paymentURL)
        webView.load(URLRequest(url: url!))
    }
    func updateMap(counter:Int){
        viewModel?.current(31, 32)
    }
    override func bind() {
        viewModel?.currentTrip.bind({ (current) in
            self.trip = current
            BaseController.currentTrip = current
            if self.trip?.id != nil {
                guard let paymentPaid = current.payment_paid , let method = current.payment_method else { return }
                if method == 2 {
                    if paymentPaid {
                        PaymentOnline.paymentSuccess = true
                        self.closePayment()
                    }
                }
            }else{
             
            }
            
        })
    }
    
    @objc func cancel(){
        self.closePayment()
    }
    func closePayment(){
        if PaymentOnline.paymentSuccess {
            self.dismiss(animated: true, completion: {
                self.delegate?.successPayment()
            })
        }else{
            self.dismiss(animated: true, completion: {
                self.delegate?.faildPayment()
            })
        }
    }
  
}
