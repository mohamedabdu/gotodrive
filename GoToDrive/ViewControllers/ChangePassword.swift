//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class ChangePassword:BaseController {
    
    @IBOutlet weak var confirmationPassword: FloatLabelTextField!
    @IBOutlet weak var newPassword: FloatLabelTextField!
    @IBOutlet weak var oldPassword: FloatLabelTextField!
    
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
    }
    override func bind() {
        viewModel?.model.bind { (data) in
            self.navigationController?.pop()
        }
    }
    override func validation() -> Bool {
        confirmationPassword.customValidationRules = [RequiredRule() , ConfirmationRule(confirmField: newPassword)]
        let validate = Validation(textFields: [oldPassword,newPassword,confirmationPassword])
        return validate.success
    }
    @IBAction func save(_ sender: Any) {
        if validation() {
            var paramters:[String:String] = [:]
            paramters["password"] = newPassword.text!
            paramters["password_confirmation"] = confirmationPassword.text!
            paramters["old_password"] = oldPassword.text!
            viewModel?.update(paramters: paramters)
        }
    }
    
}
