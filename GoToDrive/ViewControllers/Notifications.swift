//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Notifications:BaseController {
    @IBOutlet weak var notificationCollection: UITableView!
    
    var notifications:[NotificationsResult] = []
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        notifications.removeAll()
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
        notificationCollection.delegate = self
        notificationCollection.dataSource = self
        notificationCollection.reloadData()
        
        viewModel?.fetchNotifications()
    }
    override func bind() {
        viewModel?.notifications.bind({ (data) in
            self.notifications.append(contentsOf: data)
            self.notificationCollection.reloadData()
        })
    }
}
extension Notifications:UITableViewDelegate , UITableViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.didEndDragging() && viewModel!.runPaginator() {
            self.viewModel?.fetchNotifications()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: NotificationCell.self, indexPath) else { return UITableViewCell() }
        cell.model = notifications[indexPath.row]
        return cell
    }
    
    
}
