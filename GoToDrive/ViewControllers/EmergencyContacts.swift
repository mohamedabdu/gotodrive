//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import Contacts

class EmergencyContacts:BaseController {
    @IBOutlet weak var contactsCollection: UITableView!
    
    var selectedContact:Int?
    var contacts:[ContactsResult] = []
    var viewModel:ContactsViewModel?
    var loaded:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel = ContactsViewModel()
        viewModel?.delegate = self
        if !loaded {
            viewModel?.fetchData()
        }
        bind()

    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        contactsCollection.delegate = self
        contactsCollection.dataSource = self

    }
    func setupTable(withAnimate:Bool = true){
        if contacts.count == 0 {
            contactsCollection.isHidden = true
            if loaded {
                self.view.animateZoom(0.10, closure: {
                    
                })
            }
            
        }else{
            contactsCollection.isHidden = false
            self.contactsCollection.reloadData{
                if withAnimate {
                    self.contactsCollection.animate(0.20)
                }
            }
            
        }
    }
    
    override func bind() {
        viewModel?.contacts.bind({ (data) in
            self.loaded = true
            self.viewModel?.delegate = nil
            self.contacts.removeAll()
            self.contacts.append(contentsOf: data)
            self.setupTable()
            
        })
        
    }
    @IBAction func addContact(_ sender: Any) {
        let vc = pushViewController(AddContact.self)
        vc.delegate = self
        vc.oldContacts = self.contacts
        push(vc)
    }
    @IBAction func more(_ sender: Any) {
        let actions:[String:Any] = [translate("make_default"):1 , translate("delete"):2]
        createActionSheet(title: translate("more"), actions: actions) { (data) in
            guard let action = data.keys.first else { return }
            if action == translate("make_default") {
                if self.selectedContact != nil {
                    guard let id = self.contacts[self.selectedContact!].id else { return }
                    self.selectedContact = nil
                    self.viewModel?.defaultContact(contact: id)
                    self.setupTable(withAnimate: false)
                }
              
            }else{
                if self.selectedContact != nil {
                    guard let id = self.contacts[self.selectedContact!].id else { return }
                    
                    self.viewModel?.delete(contact: id)
                    self.contacts.remove(at: self.selectedContact!)
                    self.selectedContact = nil
                    self.setupTable(withAnimate: false)
                }
            }
        }
    }
}

extension EmergencyContacts:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: ContactCell.self, indexPath) else { return UITableViewCell() }
        if selectedContact != nil && selectedContact! == indexPath.row {
            cell.checked = true
        }else{
            cell.checked = false
        }
        cell.fromEmergencyContactList = true
        cell.delegate = self
        cell.model = self.contacts[indexPath.row]
        return cell
    }
    
    
}

extension EmergencyContacts:AddContactDelegate {
    func updateContacts(list: [ContactsResult]) {
        self.contacts.removeAll()
        self.contacts.append(contentsOf: list)
        self.setupTable()
    }

}

extension EmergencyContacts:ContactCellDelegate {
    func select(path: Int) {
        if selectedContact != nil && selectedContact! == path {
            selectedContact = nil
        }else {
            selectedContact = path
        }
        contactsCollection.reloadData()
    }
    func disSelect(path: Int) {
        if selectedContact != nil && selectedContact! == path {
            selectedContact = nil
        }
        contactsCollection.reloadData()
    }
}
