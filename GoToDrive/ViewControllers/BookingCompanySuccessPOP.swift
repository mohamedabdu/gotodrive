//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

protocol BookingCompanySuccessDelegate:class {
    func successApply()
    func successCancel()
}

class BookingCompanySuccessPOP:BaseController {
    @IBOutlet weak var successMessage: UILabel!
    @IBOutlet weak var POPTitle: UILabel!
    
    var word:String?
    var titleName:String? = translate("booking_successfully")
    weak var delegate:BookingCompanySuccessDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
    func setup() {
        POPTitle.text = titleName
        successMessage.text = word
    }
    override func bind() {
        
    }
    @IBAction func apply(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.successApply()
        })
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion:  {
            self.delegate?.successCancel()
        })
    }
}
