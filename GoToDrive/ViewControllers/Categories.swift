//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Categories:BaseController {
    enum CategoriesNames: String {
        case privateCar = "private_car"
        case truck
        case companies = "companies"
        case truckWater = "truck_water"
        case tanks
        case delivery
        case learning = "learning_car"
        case service
        
    }
    @IBOutlet weak var categoriesCollection: UITableView!
    
    var categories:[ConfigCategory] = []
    var categorySelected:Int = 0
    var viewModel:CategoryViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.categories.removeAll()
        self.viewModel = nil
    }
    
    func setup() {
        self.firstShow()
        self.viewModel = CategoryViewModel()
        self.viewModel?.delegate = self
        
    }
    override func notifySetting() {
        self.firstShow()
    }
    func fromSelfDetail(){
        
    }
    func firstShow(){
        if let list = BaseController.config?.categories {
            self.categories.append(contentsOf: list)
            self.categoriesCollection.delegate = self
            self.categoriesCollection.dataSource = self
            self.categoriesCollection.animate {
                self.categoriesCollection.reloadData()
            }
        }
    }
    override func bind() {
        viewModel?.model.bind({ (data) in
            let category = self.categories[self.categorySelected]
            guard let id = category.id else { return }
            let vc = self.pushViewController(SendRequest.self)
            vc.privateCategories = data
            vc.category = id
            vc.categoryModel = category
            self.push(vc)
            
        })
    }
    @IBAction func next(_ sender: Any) {
        if categories.isset(categorySelected){
            let category = categories[categorySelected]
            guard let id = category.id else { return }
            if category.calculating_pricing == CategoriesNames.privateCar.rawValue {
                /** fetch sub category**/
                self.viewModel?.fetchData(id)
                
            }else if category.calculating_pricing == CategoriesNames.truck.rawValue {
                let vc = pushViewController(SubCategories.self)
                vc.category = category
                push(vc)
            }else if category.calculating_pricing == CategoriesNames.tanks.rawValue {
                let vc = pushViewController(SubCategories.self)
                vc.category = category
                push(vc)
            }else if category.calculating_pricing == CategoriesNames.truckWater.rawValue {
                let vc = pushViewController(SubCategories.self)
                vc.category = category
                push(vc)
            }else if category.calculating_pricing == CategoriesNames.companies.rawValue {
                let vc = pushViewController(Companies.self)
                vc.category = category.id!
                push(vc)
            } else if category.calculating_pricing == CategoriesNames.delivery.rawValue {
                let vc = self.pushViewController(SendRequest.self)
                guard let id = category.id else { return }
                vc.category = id
                vc.categoryModel = category
                self.push(vc)
            } else if category.calculating_pricing == CategoriesNames.learning.rawValue {
                /** fetch sub category**/
                self.viewModel?.fetchData(id)

            }
            else if category.calculating_pricing == CategoriesNames.service.rawValue {
                /** fetch sub category**/
                self.viewModel?.fetchData(id)
                
            }
            
            
        }
    }
}

extension Categories:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: CategoryCell.self, indexPath) else { return UITableViewCell() }
        if categorySelected == indexPath.row {
            cell.checked = true
        }else{
            cell.checked = false
        }
        cell.model = self.categories[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.categorySelected = indexPath.row
        tableView.reloadData()
    }
    
}
