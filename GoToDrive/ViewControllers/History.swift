//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class History:BaseController {
    @IBOutlet weak var historyCollection: UITableView!
    
    var viewModel:UserViewModel?
    var tripViewModel:TripViewModel?
    var historyList:[TripResult] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        tripViewModel = nil
        historyList.removeAll()
    }
    
    func setup() {
        viewModel = UserViewModel()
        tripViewModel = TripViewModel()
        viewModel?.delegate = self
        historyCollection.delegate = self
        historyCollection.dataSource = self
        historyCollection.reloadData()
        
        viewModel?.historyTrips()
    }
    override func bind() {
        viewModel?.history.bind({ (data) in
            self.historyList.append(contentsOf: data)
            self.historyCollection.reloadData()
        })
    }
}

extension History:UITableViewDelegate , UITableViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.didEndDragging() && viewModel!.runPaginator() {
            self.viewModel?.historyTrips()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if historyList[indexPath.row].category_id != nil && ( historyList[indexPath.row].category_calculating_pricing == Categories.CategoriesNames.privateCar.rawValue || historyList[indexPath.row].category_calculating_pricing == Categories.CategoriesNames.truck.rawValue || historyList[indexPath.row].category_calculating_pricing == Categories.CategoriesNames.delivery.rawValue ) {
            guard var cell = tableView.cell(type: HistoryCell.self, indexPath) else { return UITableViewCell() }
            cell.model = historyList[indexPath.row]
            return cell
        } else {
            guard var cell = tableView.cell(type: HistoryCompanyCell.self, indexPath) else { return UITableViewCell() }
            cell.model = historyList[indexPath.row]
            cell.delegate = self
            return cell
        }
      
    }
}

extension History: HistoryCompanyCellDelegate {
    func finishTrip(path: Int) {
        historyList[path].status = 6
        historyList[path].status_text = translate("completed")
        historyCollection.reloadData()
        tripViewModel?.finishRequestCompany()
        BaseController.currentTrip = nil
    }
}
