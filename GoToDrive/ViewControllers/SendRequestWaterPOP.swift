//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

protocol SendRequestWaterDelegate:class {
    func successApply(paramters:[String:Any])
}

class SendRequestWaterPOP:BaseController {
        
    @IBOutlet weak var notesField: UITextField!
    @IBOutlet weak var requiredField: UITextField!
    weak var delegate:SendRequestWaterDelegate?
    var paramters:[String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
    func setup() {
       
    }
    override func bind() {
        
    }
    override func validation() -> Bool {
        let validation = Validation(textFields: [notesField , requiredField])
        return validation.success
    }
    @IBAction func apply(_ sender: Any) {
        if validation() {
            self.dismiss(animated: true) {
                self.paramters["trip_special_description"] = self.requiredField.text
                self.paramters["trip_special_note"] = self.notesField.text
                self.delegate?.successApply(paramters: self.paramters)
            }
        }
        
    }
    @IBAction func cancel(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }
}
