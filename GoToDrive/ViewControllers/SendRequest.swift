//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

enum CurrentSelection {
    case orgin
    case destination
}
class SendRequest:BaseController {
    
    @IBOutlet weak var circleGreenImage: UIImageView!
    @IBOutlet weak var viewUnderLine: UIView!
    @IBOutlet weak var dotsImage: UIImageView!
    
    @IBOutlet weak var fareEstimation: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var destinationLocation: UILabel!
    @IBOutlet weak var orginLocation: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var pormoCodeLbl: UILabel!
    
    /** private cateogires **/
    @IBOutlet weak var privateSubCategoryCollection: UICollectionView!
    @IBOutlet weak var privateCategoriesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var transsimsionView: UIView!
    @IBOutlet weak var automaticBtn: RadioButton!
    @IBOutlet weak var manualBtn: RadioButton!
    @IBOutlet weak var ageBtn: UIButton!

    @IBOutlet weak var hasCarView: UIView!
    @IBOutlet weak var hasCar: UISwitch!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    
    /** class attr */
    var viewModel:TripViewModel?
    var mapHelper:GoogleMapHelper?
    var orgin:CLLocation! = CLLocation()
    var destination:CLLocation! = CLLocation()
    var selection:CurrentSelection = .orgin
    var fareEstimate:String?
    var timer:TimeHelper?
    /** */
    
    /** categories **/
    var privateCategories:[SubCategoryResult] = []
    var categorySelected:Int?
    
    /** trip post **/
    var category:Int!
    var categoryModel:ConfigCategory?
    var subCategory:Int?
    var payment:PaymentsMethod!
    var promoCode:String?
    var cancelReason:Int?
    var rentCar:Bool = false
    var rentHours:String?
    var withOutCar: Bool = false
    var transmission: String?
    var ageFilter: Int?
    /** */
    
    /** nearest Driver attr */
    var lat:Double?
    var lng:Double?
    var drivers:[TripDriver] = []
    var oldDrivers:[TripDriver] = []
    var trip:TripResult?
    /** */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* go to pickup map before loaded */
        self.goToPickUp()
        super.viewWillAppear(animated)
        setupTrip()
        viewModel = TripViewModel()
        bind()

    }
    override func viewDidAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.tripDelegate = self
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer?.stopTimer()
        self.timer = nil
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.tripDelegate = nil
    }
    
    func setup() {
        
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapHelper = GoogleMapHelper()
        mapHelper?.mapView = mapView
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        mapHelper?.polylineDataSource = self
        self.currentLocation()
        orginLocation.text = ""
        destinationLocation.text = translate("drop_down_location")
        self.fareEstimation.text = translate("fare_estimation")
        
        guard let method = UserRoot.instance.result?.payment_method_text else { return }
        self.paymentMethod.text = method
        self.payment = PaymentsMethod(rawValue:method)
        
        /** set collection View **/
        if categoryModel?.calculating_pricing == Categories.CategoriesNames.privateCar.rawValue || categoryModel?.calculating_pricing == Categories.CategoriesNames.learning.rawValue || categoryModel?.calculating_pricing == Categories.CategoriesNames.service.rawValue  {
            self.privateSubCategoryCollection.delegate = self
            self.privateSubCategoryCollection.dataSource = self
        }
        /** **/
        
        /** check if delviery **/
        if categoryModel?.calculating_pricing == "delivery" {
            viewUnderLine.isHidden = true
            circleGreenImage.isHidden = true
            orginLocation.isHidden = true
            dotsImage.isHidden = true
        }
        if categoryModel?.calculating_pricing == Categories.CategoriesNames.learning.rawValue {
            self.hasCarView.isHidden = false
            self.transsimsionView.isHidden = false
        } else if categoryModel?.calculating_pricing == Categories.CategoriesNames.privateCar.rawValue {
            self.hasCarView.isHidden = false
            self.transsimsionView.isHidden = true
            mapViewHeight = mapViewHeight.setMultiplier(multiplier: 0.62)
        } else {
            self.transsimsionView.isHidden = true
            self.hasCarView.isHidden = true
            mapViewHeight = mapViewHeight.setMultiplier(multiplier: 0.70)
        }
        
        manualBtn.onDeselect { [weak self] in
            self?.transmission = nil
        }
        automaticBtn.onDeselect { [weak self] in
            self?.transmission = nil
        }
        automaticBtn.onSelect { [weak self] in
            self?.manualBtn.deselect()
            self?.transmission = "automatic"
        }
       
        manualBtn.onSelect { [weak self] in
            self?.automaticBtn.deselect()
            self?.transmission = "manual"
        }
    }
    func currentLocation(){
        selection = .orgin
        mapHelper?.currentLocation()
    }
    override func bind() {
        self.bindEstimate()
        self.bindDrivers()
        self.bindCreated()
        self.bindPromoCode()
    }

    func goToPickUp(){
        if self.trip?.id != nil || BaseController.currentTrip?.id != nil {
            let vc = pushViewController(PickupMap.self)
            vc.trip = self.trip
            if BaseController.currentTrip != nil && self.trip?.id == BaseController.currentTrip?.id {
                vc.trip = BaseController.currentTrip
            }
            self.dismiss(animated: true, completion: nil)
            push(vc,false)
        }
    }
    
    @IBAction func orginSearch(_ sender: Any) {
        if categoryModel?.calculating_pricing != "delivery" {
            self.selection = .orgin
            self.mapHelper?.search()
        }
    }
    @IBAction func destinationSearch(_ sender: Any) {
        self.selection = .destination
        self.mapHelper?.search()
    }
    
    @IBAction func hasCarSwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.withOutCar = true
            self.privateSubCategoryCollection.isHidden = true
        } else {
            self.withOutCar = false
            self.privateSubCategoryCollection.isHidden = false
        }
    }
}


// MARK: - UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
extension SendRequest:UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width/3-15, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return privateCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard var cell = collectionView.cell(type: PrivateSubCategoryCell.self, indexPath) else { return UICollectionViewCell () }
        if self.categorySelected != nil && self.categorySelected! == indexPath.row {
            cell.categorySelected = true
        }else{
            cell.categorySelected = false
        }
        cell.model = privateCategories[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.categorySelected = indexPath.row
        self.privateSubCategoryCollection.reloadData{
            self.subCategory = self.privateCategories[indexPath.row].id
            self.estimateTrip()
        }
    }
    
}

// MARK: - neaest drivers functions
extension SendRequest {
    func setupTrip(){
       timer = TimeHelper(seconds: 8, closure: updateMap)
        
    }
    func updateMap(counter:Int){
        if self.lat != nil && self.lng != nil {
            viewModel?.delegate = nil
            viewModel?.current(lat!, lng!)
        }
    }
    func bindDrivers(){
        viewModel?.drivers.bind({ (drivers) in
            if self.oldDrivers.count == 0 {
                self.oldDrivers.append(contentsOf: drivers)
            }else{
                self.oldDrivers.removeAll()
                self.oldDrivers.append(contentsOf: self.drivers)
            }
            self.drivers.removeAll()
            self.drivers.append(contentsOf: drivers)
            
            self.mapHelper?.refreshARMovement(lat: self.lat, lng: self.lng)
            self.drawPath()
        })
    }
}

// MARK: - trips functions
extension SendRequest {
    func drawPath()
    {
        self.mapHelper?.drawRoute(orgin: orgin, destination: destination)
    }
    func estimateTrip(){
        var fromLat = orgin.coordinate.latitude
        var fromLng = orgin.coordinate.longitude
        var toLat = destination.coordinate.latitude
        var toLng = destination.coordinate.longitude
        
        fromLat = Double(fromLat)
        fromLng = Double(fromLng)
        toLat = Double(toLat)
        toLng = Double(toLng)
        self.viewModel?.estimate(from_lat: fromLat, from_lng: fromLng, to_lat: toLat, to_lng: toLng)
    }
    func bindEstimate(){
        viewModel?.estimateTrip.bind({ (trip) in
            self.fareEstimate = trip.fare_estimation
            self.fareEstimation.text = "\(trip.fare_estimation ?? "") \(UserRoot.instance.result?.currency ?? "")"
        })
    }
    @IBAction func confirmRequest(_ sender: Any) {
        if categoryModel?.calculating_pricing == Categories.CategoriesNames.truckWater.rawValue {
            self.confirmRequestWater()
            return
        } else if categoryModel?.calculating_pricing == Categories.CategoriesNames.delivery.rawValue {
            self.confirmRequestDelivery()
            return
        }
        if  orgin.coordinate.latitude != 0 && orgin.coordinate.longitude != 0 && category != nil  {
            self.viewModel?.delegate = self
            var paramters:[String:Any] = [:]
            paramters["category_id"] = category
            if subCategory != nil {
                paramters["sub_category_id"] = subCategory
            }
            paramters["payment_method"] = payment.rawValue
            paramters["promo_code"] = promoCode
            paramters["from_lat"] = orgin.coordinate.latitude
            paramters["from_lng"] = orgin.coordinate.longitude
            if destination.coordinate.latitude != 0 && destination.coordinate.longitude != 0 {
                paramters["to_lat"] = destination.coordinate.latitude
                paramters["to_lng"] = destination.coordinate.longitude
            }
            
            //** rent car **//
            if rentCar && rentHours != nil {
                paramters["type"] = "private"
                paramters["number_of_hours"] = rentHours!
            }
            if withOutCar {
                paramters["without_car"] = true
            }
            if categoryModel?.calculating_pricing == Categories.CategoriesNames.learning.rawValue {
                if transmission == nil {
                    showAlert(message: translate("the_transmission_is_required"))
                    return
                }
                paramters["car_transmission"] = transmission
            }
            paramters["request_age"] = self.ageFilter
            self.viewModel?.create(paramters: paramters)
        }
       
    }
    @IBAction func ageAction(_ sender: Any) {
        
        let vc = pushViewController(PickerViewController.self)

        vc.source.append(contentsOf: BaseController.config?.ages ?? [])
        let model = AgesData()
        model.title = translate("all")
        vc.source.append(model)
        vc.titleClosure = { row in
            guard let model = vc.source[row] as? AgesData else { return "" }
            if model.id == nil {
                return model.title
            } else {
                return "\(model.from ?? 0) - \(model.to ?? 0)"
            }
        }
        vc.didSelectPath = { [weak self] row in
            guard let model = vc.source[row] as? AgesData else { return }
            var title = ""
            if model.id == nil {
                title = translate("all")
            } else {
                title  = "\(model.from ?? 0) - \(model.to ?? 0)"
                self?.ageFilter = model.id
            }
            self?.ageBtn.setTitle(title, for: .normal)
        }
        pushPop(vc: vc)
    }
    
    func confirmRequestWater(){
        if  category != nil  {
            var paramters:[String:Any] = [:]
            paramters["category_id"] = category
            if subCategory != nil {
                paramters["sub_category_id"] = subCategory
            }
            paramters["payment_method"] = payment.rawValue
            paramters["promo_code"] = promoCode
            paramters["from_lat"] = orgin.coordinate.latitude
            paramters["from_lng"] = orgin.coordinate.longitude
            if destination.coordinate.latitude != 0 && destination.coordinate.longitude != 0 {
                paramters["to_lat"] = destination.coordinate.latitude
                paramters["to_lng"] = destination.coordinate.longitude
            }
         
            let vc = pushViewController(SendRequestWaterPOP.self)
            vc.delegate = self
            vc.paramters = paramters
            pushPop(vc: vc)
            
        }
        
    }
    func confirmRequestDelivery() {
        if  category != nil  {
            var paramters:[String:Any] = [:]
            paramters["category_id"] = category
            if subCategory != nil {
                paramters["sub_category_id"] = subCategory
            }
            paramters["payment_method"] = payment.rawValue
            paramters["promo_code"] = promoCode
            if destination.coordinate.latitude != 0 && destination.coordinate.longitude != 0 {
                paramters["to_lat"] = destination.coordinate.latitude
                paramters["to_lng"] = destination.coordinate.longitude
            } else {
                paramters["to_lat"] = lat
                paramters["to_lng"] = lng
            }
            let vc = pushViewController(SendRequestDeliveryPOP.self)
            vc.delegate = self
            vc.paramters = paramters
            pushPop(vc: vc)
        }
    }
    func bindCreated(){
        viewModel?.created.bind({ (data) in
            if data.id != nil {
                self.stopLoading()
                self.viewModel?.delegate = nil
                self.trip = data
                BaseController.currentTrip = data
                if self.categoryModel?.calculating_pricing == Categories.CategoriesNames.truckWater.rawValue {
                    let vc = self.pushViewController(BookingCompanySuccessPOP.self)
                    vc.delegate = self
                    vc.word = translate("the company will respond to your request for the price and is required by you to implement")
                    self.pushPop(vc: vc)
                }else{
//                    let vc = self.pushViewController(BookingSuccessPOP.self)
//                    vc.delegate = self
//                    vc.estimateTime = self.trip?.statistics?.arriving_minutes
//                    self.pushPop(vc: vc)
                    let vc = self.pushViewController(Home.self)
                    self.push(vc)
                }
            }
        })
    }
    
}


// MARK: - GoogleMapHelperDelegate
extension SendRequest: GoogleMapHelperDelegate {
    func locationCallback(lat: Double, lng: Double) {
        if selection == .orgin {
            self.lat = lat
            self.lng = lng
            self.updateMap(counter: 0)
            orgin = CLLocation(latitude: lat, longitude: lng)
        }else{
            destination = CLLocation(latitude: lat, longitude: lng)
        }
        if destination.coordinate.latitude != 0 {
            drawPath()
            self.estimateTrip()
        }
    
    }
    func locationCallback(address: String?) {
        if selection == .orgin {
            self.orginLocation.text = address
        }else{
            self.destinationLocation.text = address
        }
    }
}

// MARK: - PolylineDataSource
extension SendRequest: PolylineDataSource {
    func polyline() -> PolylineAttrbuite {
        var poly = PolylineAttrbuite()
        poly.width = 2
        poly.color = .blue
        return poly
    }

}

// MARK: - MarkerDataSource
extension SendRequest:MarkerDataSource {
    func checkInOldDrivers(_ driver:TripDriver)->Int? {
        let indexes = oldDrivers.indices(of: driver)
        return indexes.first
    }
    func marker() -> MarkerAttrbuite {
        var attr = MarkerAttrbuite()
        attr.use = .icon
        attr.icon = #imageLiteral(resourceName: "pinColored")
        return attr
    }
    func setMarkers() -> [GMSMarker] {
        var markers:[GMSMarker] = []
        let imageView = UIImageView()
        drivers.forEach { (data) in
            let marker = GMSMarker()
            marker.icon = #imageLiteral(resourceName: "carColored")
            if imageView.image == nil {
                imageView.setImage(url: drivers.first?.category_icon) {
                    marker.icon = imageView.image?.imageResize(CGSize(width: 25, height: 25))
                }
            }
            if let old = checkInOldDrivers(data)  {
                guard let lat = oldDrivers[old].lat?.double() ,  let lng = oldDrivers[old].lng?.double() else { return }
                marker.oldPosition = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }else{
                guard let lat = data.lat?.double() ,  let lng = data.lng?.double() else { return }
                marker.oldPosition = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }
            guard let lat = data.lat?.double() ,  let lng = data.lng?.double() else { return }
            marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            
            markers.append(marker)
        }
        return markers
    }
}


// MARK: - PromoCodeDelegate
extension SendRequest: PromoCodeDelegate {
    @IBAction func promoCode(_ sender: Any) {
        let vc = pushViewController(PromoCodePOP.self)
        vc.delegate = self
        pushPop(vc: vc)
    }
    func agree(code: String?) {
        self.promoCode = code
        if let code = code {
            self.viewModel?.promoCode(code: code)
        }
    }
    
    func cancel() {
        
    }
    func bindPromoCode() {
        viewModel?.promoCodeHandler.bind({ (data) in
            self.pormoCodeLbl.text = "\(translate("promo_code")) \(data.discount?.string ?? "") %"
        })
    }
}


// MARK: - ChangePaymentDelegate
extension SendRequest: ChangePaymentDelegate {
    @IBAction func changePayment(_ sender: Any) {
        let vc = pushViewController(ChangePaymentMethod.self)
        vc.delegate = self
        vc.fareEstimate = self.fareEstimate
        vc.currentMethod = payment
        push(vc)
    }
    func done(payment: PaymentsMethod) {
        self.payment = payment
        self.paymentMethod.text = self.payment.rawValue
    }
}

// MARK: - BookingSuccessDelegate
extension SendRequest: BookingSuccessDelegate {
    func apply() {
        let vc = pushViewController(ConfirmRequest.self)
        vc.orgin = orginLocation.text
        vc.destination = destinationLocation.text
        push(vc)
    }
    func successCancel(){
        let vc = pushViewController(Home.self)
        push(vc)
    }
}

// MARK: - SendRequestWaterDelegate , BookingCompanySuccessDelegate
extension SendRequest:SendRequestWaterDelegate , BookingCompanySuccessDelegate {
    func successApply() {
        let vc = pushViewController(Home.self)
        push(vc)
    }
    
    func successApply(paramters: [String : Any]) {
        self.viewModel?.delegate = self
        self.viewModel?.createRequestCompany(paramters: paramters)
    }
}

// MARK: - SendRequestDeliveryDelegate
extension SendRequest: SendRequestDeliveryDelegate {
    func deliveryNote(paramters: [String : Any]) {
        self.viewModel?.delegate = self
        self.viewModel?.create(paramters: paramters)
    }
}


// MARK: - TripDelegate
extension SendRequest: TripDelegate {
    func didReceive(trip: TripResult?) {
        guard let trip = trip else { return }
        self.trip = trip
        BaseController.currentTrip = trip
        goToPickUp()
    }
}

