//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

protocol AddContactDelegate:class {
    func updateContacts(list:[ContactsResult])
}
class AddContact:BaseController {
    @IBOutlet weak var contactsCollection: UITableView!
    
    var oldContacts:[ContactsResult] = []
    var contacts:[ContactsResult] = []
    var selectedContacts:[Int] = []
    var viewModel:ContactsViewModel?
    weak var delegate:AddContactDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        contacts.removeAll()
    }
    
    func setup() {
        viewModel = ContactsViewModel()
        viewModel?.delegate = self
        self.filterCollection()
        contactsCollection.dataSource = self
        contactsCollection.delegate = self
        contactsCollection.reloadData()
        
    }
    func filterCollection(){
        let contactHelper = ContactsHelper()
        let deviceContacts = contactHelper.contacts()
        
        for i in 0 ..< deviceContacts.count {
            self.oldContacts.forEach { (item) in
                if deviceContacts[i].mobile == item.mobile && deviceContacts[i].name == item.name {
                    self.selectedContacts.append(i)
                }
            }
        }
        self.contacts.append(contentsOf: deviceContacts)
    }
    override func bind() {
        viewModel?.contacts.bind({ (data) in
            self.navigationController?.popViewController({
                self.delegate?.updateContacts(list: data)
            })
        })
    }
    @IBAction func edit(_ sender: Any) {
        if selectedContacts.count == 0 {
            self.SnackBar(message: translate("please_select_at_least_one"),duration: .short)
            return
        }
        let contactModel = ContactsModel()
        var list:[ContactsResult] = []
        selectedContacts.forEach { (path) in
            if contacts.isset(path) {
                list.append(contacts[path])
            }
        }
        contactModel.result = list
        let json = contactModel.convertToJson()
        if let data = json {
            guard let string = String(data: data, encoding: .utf8) else { return }
            viewModel?.create(json: string)
        }
        
    }
    @IBAction func search(_ sender: Any) {
    }
}

extension AddContact:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: ContactCell.self, indexPath) else { return UITableViewCell() }
        if selectedContacts.contains(indexPath.row){
            cell.checked = true
        }else{
            cell.checked = false
        }
        cell.delegate = self
        cell.model = self.contacts[indexPath.row]
        return cell
    }
    
    
}

extension AddContact:ContactCellDelegate {
    func select(path: Int) {
        selectedContacts.append(path)
    }
    
    func disSelect(path: Int) {
        if selectedContacts.contains(path) {
            for i in 0..<selectedContacts.count {
                if selectedContacts[i] == path {
                    selectedContacts.remove(at: i)
                    break
                }
            }
        }
    }
    
    
}
