//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

protocol RentCarDelegate:class {
    func agree(hours:String?)
    func cancel()
}
class RentCarPOP:BaseController {
    @IBOutlet weak var code: UITextField!
    
    weak var delegate:RentCarDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
    func setup() {
        
    }
    override func bind() {
        
    }
    
    @IBAction func apply(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.agree(hours: self.code.text)
        }
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
