//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

class SendRequestWater:BaseController {
 
    @IBOutlet weak var destinationLocation: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    /** class attr */
    var viewModel:TripViewModel?
    var mapHelper:GoogleMapHelper?
    var destination:CLLocation! = CLLocation()
    var timer:TimeHelper?
    /** */
    
    /** trip post **/
    var categoryModel: ConfigCategory?
    var category:Int!
    var subCategory:Int?
    var cancelReason:Int?
    /** */
    
    /** nearest Driver attr */
    var lat:Double?
    var lng:Double?
    var trip:TripResult?
    /** */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* go to pickup map before loaded */
        self.goToPickUp()
        super.viewWillAppear(animated)
        setupTrip()
        viewModel = TripViewModel()
        bind()

    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer?.stopTimer()
        self.timer = nil
    }
    
    func setup() {
        
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapHelper = GoogleMapHelper()
        mapHelper?.mapView = mapView
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        mapHelper?.polylineDataSource = self
        mapHelper?.currentLocation()
        destinationLocation.text = translate("drop_down_location")
    
    }
    
    override func bind() {
        self.bindCreated()
    }
    func goToPickUp(){
        if self.trip?.id != nil || BaseController.currentTrip?.id != nil {
            let vc = pushViewController(PickupMap.self)
            vc.trip = self.trip
            if BaseController.currentTrip != nil && self.trip?.id == BaseController.currentTrip?.id {
                vc.trip = BaseController.currentTrip
            }
            push(vc,false)
        }
    }
    
    @IBAction func destinationSearch(_ sender: Any) {
        self.mapHelper?.search()
    }
    

    
}

/** nearest drivers functions */
extension SendRequestWater {
    func setupTrip(){
        timer = TimeHelper(seconds: 8, closure: updateMap)
        
    }
    func updateMap(counter:Int){
        if self.lat != nil && self.lng != nil {
            viewModel?.delegate = nil
            viewModel?.current(lat!, lng!)
        }
    }
  
}
/** trips functions */
extension SendRequestWater {
    
    @IBAction func confirmRequest(_ sender: Any) {
       
        if  category != nil  {
            
            var paramters:[String:Any] = [:]
            paramters["category_id"] = category
            if subCategory != nil {
                paramters["sub_category_id"] = subCategory
            }
            if let method = UserRoot.instance.result?.payment_method_text{
                paramters["payment_method"] = PaymentsMethod(rawValue:method)?.rawValue
            }
            if destination.coordinate.latitude != 0 && destination.coordinate.longitude != 0 {
                paramters["to_lat"] = destination.coordinate.latitude
                paramters["to_lng"] = destination.coordinate.longitude
            }
        
            let vc = pushViewController(SendRequestWaterPOP.self)
            vc.delegate = self
            vc.paramters = paramters
            pushPop(vc: vc)
            
        }
        
        
    }
    func bindCreated(){
        viewModel?.created.bind({ (data) in
            self.stopLoading()
            self.viewModel?.delegate = nil
            self.trip = data
            BaseController.currentTrip = data
            let vc = self.pushViewController(BookingCompanySuccessPOP.self)
            vc.delegate = self
            vc.word = translate("the company will respond to your request for the price and is required by you to implement")
            self.pushPop(vc: vc)
        })
    }
    
}

extension SendRequestWater:GoogleMapHelperDelegate{
    func locationCallback(lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
        self.updateMap(counter: 0)
        destination = CLLocation(latitude: lat, longitude: lng)
        self.mapHelper?.updateCamera(lat: lat, lng: lng)
    }
    func locationCallback(address: String?) {
        self.destinationLocation.text = address
    }
}

extension SendRequestWater:PolylineDataSource{
    func polyline() -> PolylineAttrbuite {
        var poly = PolylineAttrbuite()
        poly.width = 2
        poly.color = .blue
        return poly
    }

}

extension SendRequestWater:MarkerDataSource {
 
    func marker() -> MarkerAttrbuite {
        var attr = MarkerAttrbuite()
        attr.use = .icon
        attr.icon = #imageLiteral(resourceName: "pinColored")
        return attr
    }
   
}



/** confirmation */
extension SendRequestWater:BookingCompanySuccessDelegate {
    func successApply() {
        let vc = pushViewController(Home.self)
        push(vc)
    }
    func successCancel(){
        let vc = pushViewController(Home.self)
        push(vc)
    }
}

extension SendRequestWater:SendRequestWaterDelegate {
    func successApply(paramters:[String:Any]) {
        self.viewModel?.delegate = self
        self.viewModel?.createRequestCompany(paramters: paramters)
    }
    
    
}
