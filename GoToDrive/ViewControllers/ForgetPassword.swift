//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class ForgetPassword:BaseController {
    @IBOutlet weak var email: FloatLabelTextField!
    
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
    }
    override func bind() {
        viewModel?.message.bind({ (data) in
            let vc = self.pushViewController(ResetPassword.self)
            vc.email = self.email.text!
            self.push(vc)
        })
    }
    override func validation() -> Bool {
        let validate = Validation(textFields: [email])
        return validate.success
    }
    @IBAction func forget(_ sender: Any) {
        if validation() {
            viewModel?.forget(email: email.text!)
        }
    }
}
